mapElement = document.createElement \div
  ..id = 'map'
ig.containers.base.appendChild mapElement
map = L.map do
  * mapElement
  * minZoom: 6,
    maxZoom: 14,
    zoom: 8,
    center: [49.78, 15.5]
    maxBounds: [[48.3,11.6], [51.3,19.1]]

baseLayer = L.tileLayer do
  * "https://samizdat.cz/tiles/ton_b1/{z}/{x}/{y}.png"
  * zIndex: 1
    opacity: 1
    attribution: 'mapová data &copy; přispěvatelé <a target="_blank" href="http://osm.org">OpenStreetMap</a>, obrazový podkres <a target="_blank" href="http://stamen.com">Stamen</a>, <a target="_blank" href="https://samizdat.cz">Samizdat</a>'

labelLayer = L.tileLayer do
  * "https://samizdat.cz/tiles/ton_l2/{z}/{x}/{y}.png"
  * zIndex: 3
    opacity: 0.75

baseLayer.addTo map
labelLayer.addTo map
markers = new L.MarkerClusterGroup!
# ig.data.kontroly .= slice 3, 4
L.Icon.Default.imagePath = "https://samizdat.cz/tools/leaflet/images/"
mm = null
ig.data.kontroly.forEach (point) ->
  content = "<ul>"
  for zavada in point.zavady
    content += "<li><span class='date'>#{zavada.1}: </span>#{zavada.0}</li>"
  content += "</ul>"
  mm := m = new L.Marker [point.y, point.x]
    ..bindPopup "<h3>#{point.kontrolovany}</h3>
      <span class='address'>#{point.adresa}</span><div class='zavady'>#content</div>"
  markers.addLayer m
map.addLayer markers
# mm.openPopup!

geocoder = new ig.Geocoder ig.containers.base
  ..on \latLng (latlng) ->
    map.setView latlng, 12
