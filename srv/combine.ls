require! {
  fs
  parse: "csv-parse"
}
lines = []
records = {}

stream = fs.createReadStream "#__dirname/../data/szpi_masne_vyrobky_coded.tsv"
reader = parse {delimiter: "\t"}
stream.pipe reader
reader.on \data (line) ->
  [id, nazev, kontrolovany, adresa, ic, kategorie, vyrobce, zeme_puvodu, zeme_vyroby, datum_kontroly, detail, parametry, x, y] = line
  x = x.replace ',' '.'
  y = y.replace ',' '.'
  if x.length > 7 => x.length = 7
  if y.length > 7 => y.length = 7
  x = parseFloat x
  y = parseFloat y
  detail .= replace /\n_x000D_/g ''
  detail .= replace /_x000D_/g ''
  detail .= replace /^\n/ ''
  detail .= replace /\n$/ ''
  detail = nazev + ": " + detail
  lines.push {id, kontrolovany, adresa, ic, kategorie, vyrobce, zeme_puvodu, zeme_vyroby, datum_kontroly, detail, parametry, x, y}
<~ reader.on \end
lines.shift!

for {kontrolovany, x, y, adresa, detail, datum_kontroly} in lines
  continue unless x and y
  id = x + "-" + y + '-' + kontrolovany
  if records[id] is void
    zavady = []
    records[id] = {kontrolovany, x, y, adresa, zavady}

  records[id].zavady.push [detail,datum_kontroly]

stream = fs.createReadStream "#__dirname/../data/veterina_kontroly_all_new.tsv"
reader = parse {delimiter: "\t"}
stream.pipe reader
lines.length = 0
reader.on \data (line) ->
  [id, x, y, adresa, bod_id, cz, ddata, kontrolovany, rok, typ_x, typ_id, zdroj, datum_kontroly, nazev_y, pocet_akci, pocet_akci_pozit, detail, typ_y] = line
  if x.length > 7 => x.length = 7
  if y.length > 7 => y.length = 7
  x = parseFloat x
  y = parseFloat y
  if datum_kontroly and id != "id"
    lines.push {kontrolovany, x, y, adresa, detail, datum_kontroly}
<~ reader.on \end

for {kontrolovany, x, y, adresa, detail, datum_kontroly} in lines
  continue unless x and y
  id = x + "-" + y + '-' + kontrolovany
  if records[id] is void
    zavady = []
    records[id] = {kontrolovany, x, y, adresa, zavady}

  records[id].zavady.push [detail,datum_kontroly]

out = for id, data of records
  data
console.log out.length
fs.writeFile "#__dirname/../data/data.json", JSON.stringify out
